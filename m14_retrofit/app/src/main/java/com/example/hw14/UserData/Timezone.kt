package com.example.hw14.UserData

data class Timezone(
    val description: String,
    val offset: String
)