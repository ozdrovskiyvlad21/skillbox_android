package com.example.hw14.UserData

data class Dob(
    val age: Int,
    val date: String
)