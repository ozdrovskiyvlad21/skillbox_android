package com.example.hw14.UserData

data class Name(
    val first: String,
    val last: String,
    val title: String
)