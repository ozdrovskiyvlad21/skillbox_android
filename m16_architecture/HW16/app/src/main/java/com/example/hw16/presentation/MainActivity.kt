package com.example.hw16.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.hw16.R
import com.example.hw16.data.UsefulActivitiesRepository_Factory.newInstance
import dagger.hilt.android.AndroidEntryPoint
import javax.xml.validation.SchemaFactory.newInstance

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, MainFragment.newInstance())
                .commitNow()
        }
    }
}