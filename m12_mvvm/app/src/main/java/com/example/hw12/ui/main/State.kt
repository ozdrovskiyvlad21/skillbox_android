package com.example.hw12.ui.main

sealed class State {
    object Loading : State()
    object Success : State()
    data class Error(
        val lessThenThreeLettersError: String?
    ) : State()
}
