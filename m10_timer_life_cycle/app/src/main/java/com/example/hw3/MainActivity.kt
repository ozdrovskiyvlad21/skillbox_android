package com.example.hw3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import com.example.hw3.databinding.ActivityMainBinding
import kotlinx.coroutines.*

class MainActivity : AppCompatActivity() {

    var job: Job? = null
    private var mainCount = 0

    private var timerIsActive = false

    private var COUNT = "COUNT"
    private var ACTIVE = "ACTIVE"

    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (savedInstanceState != null) {
            timerIsActive = savedInstanceState.getBoolean(ACTIVE)
            mainCount = savedInstanceState.getInt(COUNT)
            if (timerIsActive) {
                startTimer(mainCount)
                binding.slider.isEnabled = false
            }
        } else {
            mainCount = binding.slider.value.toInt()
            binding.count.text = mainCount.toString()
        }


        binding.slider.addOnChangeListener { _, value, _ ->
            if(binding.slider.isEnabled) {
                binding.count.text = value.toInt().toString()
                mainCount = value.toInt()
            }
        }



        binding.start.setOnClickListener {
            startTimer(mainCount)
        }

    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putInt(COUNT, mainCount)
            putBoolean(ACTIVE, timerIsActive)
        }
        super.onSaveInstanceState(outState)
    }

    private fun startTimer(time: Int) {
        if (job != null) {
            job?.cancel()
            job = null
            binding.start.text = "Start"
            timerIsActive = false
        } else {
            binding.slider.isEnabled = false
            binding.start.text = "Stop"
            timerIsActive = true
            job = MainScope().launch {
                for (i in 0..mainCount) {
                    mainCount -= 1
                    binding.progress.progress -= 100 / time
                    binding.count.text = mainCount.toString()
                    if (mainCount <= 0) {
                        cancel()
                        binding.slider.isEnabled = true
                        binding.progress.progress = 100
                        binding.count.text = mainCount.toString()
                        binding.start.text = "Start"
                        job = null
                        timerIsActive = false
                        mainCount = binding.slider.value.toInt()
                    }
                    binding.count.text = mainCount.toString()
                    delay(1000)
                }
            }
        }
    }

}