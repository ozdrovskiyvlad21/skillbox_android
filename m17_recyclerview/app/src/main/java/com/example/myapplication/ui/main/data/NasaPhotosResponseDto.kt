package com.example.myapplication.ui.main.data

import com.example.myapplication.ui.main.entity.NasaMarsPhotosList
import com.example.myapplication.ui.main.entity.NasaPhotosResponse

class NasaPhotosResponseDto(
    override val photos: List<NasaMarsPhotosList>
) : NasaPhotosResponse
