package com.example.hw14.UserData

data class Id(
    val name: String,
    val value: String
)