package com.example.hw14.UserData

data class Coordinates(
    val latitude: String,
    val longitude: String
)