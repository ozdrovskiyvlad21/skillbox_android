package com.example.hw15.ui.main

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModel
import androidx.lifecycle.lifecycleScope
import com.example.hw15.MainActivity
import com.example.hw15.R
import com.example.hw15.databinding.FragmentMainBinding
import kotlinx.coroutines.flow.collect

class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!
    private val viewModel: MainViewModel by viewModels {
        object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                val dictionaryDao = (activity as MainActivity).dictionaryDao
                return MainViewModel(dictionaryDao) as T
            }
        }
    }

    companion object {
        fun newInstance() = MainFragment()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        lifecycleScope.launchWhenStarted {
            viewModel.allWords
                .collect { words ->
                    binding.infoFromDatabase
                        .text = words.joinToString(
                        separator = "\r\n"
                    )
                }
        }

        binding.addButton.setOnClickListener {
            viewModel.addWord(binding.add.text.toString())
        }
        binding.deleteButton.setOnClickListener {
            viewModel.deleteAll()
        }
    }

}