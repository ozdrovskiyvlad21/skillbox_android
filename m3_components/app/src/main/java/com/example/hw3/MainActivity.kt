package com.example.hw3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import com.example.hw3.databinding.ActivityMainBinding
import com.google.android.material.slider.Slider
import kotlinx.coroutines.*
import kotlin.concurrent.thread

class MainActivity : AppCompatActivity() {

    var job:Job? = null

    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        var count = binding.slider.value.toInt()

        binding.slider.addOnChangeListener{ _, value, _ ->
            binding.count.text = value.toInt().toString()
            count = value.toInt()
        }

        binding.start.setOnClickListener {
            val startValue = count
            if(job != null){
                job?.cancel()
                job = null
                binding.count.text = startValue.toString()
                binding.start.text = "Start"
            }
            else{
                binding.slider.isEnabled = false
                binding.start.text = "Stop"
                job = MainScope().launch {
                    for(i in 0..count){
                        count -= 1
                        binding.progress.progress -= 100/startValue
                        binding.count.text = count.toString()
                        if(count <= 0){
                            cancel()
                            binding.slider.isEnabled = true
                            binding.progress.progress = 100
                            binding.count.text = count.toString()
                            binding.start.text = "Start"
                            job = null
                        }
                        binding.count.text = count.toString()
                        delay(1000)
                    }
                }
            }

        }


    }
}