package com.example.hw11

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences

private const val PREFERENCE_NAME = "prefs_name"
private const val SHARED_PREFERENCE_KEY = "shared_preference_key"

private lateinit var prefs: SharedPreferences


class Repository {

    var localValue: String? = null

    private fun getDataFromSharedPreference(context: Context): String? {
        prefs = context.getSharedPreferences(PREFERENCE_NAME, MODE_PRIVATE)
        return prefs.getString(SHARED_PREFERENCE_KEY, "")
    }

    private fun getDataFromLocalVariable(): String? {
        return localValue
    }

    fun saveText(text: String) {
        prefs.edit().putString(SHARED_PREFERENCE_KEY, text).apply()
        localValue = text
    }

    fun clearText() {
        prefs.edit().clear().apply()
        localValue = null
    }

    fun getText(context: Context): String {
        return when{
            getDataFromSharedPreference(context) != null -> getDataFromSharedPreference(context)!!
            getDataFromLocalVariable() != null -> getDataFromLocalVariable()!!
            else -> ""
        }
    }
}