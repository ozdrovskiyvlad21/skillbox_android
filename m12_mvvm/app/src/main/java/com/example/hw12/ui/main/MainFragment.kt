package com.example.hw12.ui.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.hw12.databinding.FragmentMainBinding
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.flow.collect

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private val viewModel: MainViewModel by viewModels()
    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.button.setOnClickListener {
            val searchText = binding.search.text.toString()
            viewModel.onButtonClickAction(searchText)
        }

        viewLifecycleOwner.lifecycleScope
            .launchWhenCreated {
                viewModel.state.collect { state ->
                    when (state) {
                        State.Loading -> {
                            binding.progress.isVisible = true
                            binding.searchLayout.error = null
                            binding.button.isEnabled = false
                        }
                        State.Success -> {
                            binding.progress.isVisible = false
                            binding.searchLayout.error = null
                            binding.button.isEnabled = true
                            binding.searchResult.text = viewModel.finalMessage
                        }
                        is State.Error -> {
                            binding.progress.isVisible = false
                            binding.searchLayout.error = state.lessThenThreeLettersError
                            binding.button.isEnabled = true
                        }
                    }
                }
            }

        viewLifecycleOwner.lifecycleScope
            .launchWhenStarted {
                viewModel.error.collect{ message->
                    Snackbar.make(requireView(), message, Snackbar.LENGTH_SHORT).show()
                }
            }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }


}