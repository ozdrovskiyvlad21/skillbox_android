package com.example.myapplication.ui.main.presentation

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.myapplication.R
import com.example.myapplication.databinding.FragmentMainBinding
import com.example.myapplication.ui.main.domain.NasaInfoUseCase
import com.example.myapplication.ui.main.entity.NasaMarsPhotosList
import com.example.myapplication.ui.main.presentation.marsPhotosList.MarsPhotosAdapter
import com.example.myapplication.ui.main.presentation.marsPhotosList.PhotoPaged
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
class MainFragment : Fragment() {

    @Inject
    lateinit var useCase: NasaInfoUseCase

    @Inject
    lateinit var paged: PhotoPaged

    private var _binging: FragmentMainBinding? = null
    private val binding get() = _binging!!

    private val viewModel: MainViewModel by viewModels()

    private val adapter = MarsPhotosAdapter { photo -> onItemClick(photo) }

    companion object {
        fun newInstance() = MainFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binging = FragmentMainBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recycler.let {
            it.adapter = adapter
            it.layoutManager =
                GridLayoutManager(requireContext(), 2, GridLayoutManager.VERTICAL, false)
        }

        viewModel.marsPhotos.onEach {
            adapter.getData(it)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.loadPhotos(useCase)

        binding.refresh.setOnRefreshListener {
            paged.load()
        }

    }

    private fun onItemClick(item: NasaMarsPhotosList) {
        val bundle = Bundle()
        bundle.putString("url", item.img_src)
        findNavController().navigate(R.id.viewPhotosFragment, bundle)

    }

}