package com.example.hw14

import com.example.hw14.UserData.UserInfo
import com.squareup.moshi.Moshi
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET

private const val BASE_URL = "https://randomuser.me/api/"

object RetrofitInstance {
    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(MoshiConverterFactory.create())
        .build()

    val searchApi: SearchApi = retrofit.create(
        SearchApi::class.java
    )
}

interface SearchApi{
    @GET(BASE_URL)
   suspend fun getUserInfo(): Response<UserInfo>
}
