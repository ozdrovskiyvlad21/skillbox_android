package com.example.hw14.UserData

data class Picture(
    val large: String,
    val medium: String,
    val thumbnail: String
)