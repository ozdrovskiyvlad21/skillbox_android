package com.example.hw14.UserData

data class Info(
    val page: Int,
    val results: Int,
    val seed: String,
    val version: String
)