package com.example.myapplication.ui.main.presentation

import android.content.SharedPreferences
import android.net.sip.SipManager.newInstance
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import com.example.myapplication.R
import com.example.myapplication.ui.main.data.NasaPhotosRepository_Factory.newInstance
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onSupportNavigateUp(): Boolean {
        return super.onSupportNavigateUp()
        val navController = findNavController(R.id.navigation)
        return navController.navigateUp()
    }

}