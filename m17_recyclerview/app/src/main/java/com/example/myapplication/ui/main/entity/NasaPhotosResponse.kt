package com.example.myapplication.ui.main.entity

interface NasaPhotosResponse {
    val photos: List<NasaMarsPhotosList>
}