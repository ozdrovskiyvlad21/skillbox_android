package com.example.myapplication.ui.main

import com.example.myapplication.ui.main.data.NasaPhotosRepository
import com.example.myapplication.ui.main.domain.NasaInfoUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Provides
    fun provideNasaInfoUseCase(): NasaInfoUseCase {
        return NasaInfoUseCase(provideNasaPhotosRepository())
    }

    @Provides
    fun provideNasaPhotosRepository(): NasaPhotosRepository {
        return NasaPhotosRepository()
    }
}