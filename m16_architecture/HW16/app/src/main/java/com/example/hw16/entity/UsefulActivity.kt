package com.example.hw16.entity

interface UsefulActivity {
    val accessibility: Double
    val activity: String
    val key: String
    val link: String
    val participants: Double
    val price: Double
    val type: String
}