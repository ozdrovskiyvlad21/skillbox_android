package com.example.hw16.data

import com.example.hw16.entity.UsefulActivity
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import javax.inject.Inject

private const val BASE_URL = "https://www.boredapi.com"

class UsefulActivitiesRepository @Inject constructor(){
    suspend  fun getUsefulActivity():UsefulActivity{
        return RetrofitServices.searchApi.getUsefulActivity()
    }
}

interface SearchApi{
    @GET ("/api/activity")
    suspend fun getUsefulActivity():UsefulActivityDto
}

object RetrofitServices {
    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(MoshiConverterFactory.create())
        .build()

    val searchApi: SearchApi = retrofit.create(SearchApi::class.java)
}
