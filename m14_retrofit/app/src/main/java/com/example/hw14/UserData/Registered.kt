package com.example.hw14.UserData

data class Registered(
    val age: Int,
    val date: String
)