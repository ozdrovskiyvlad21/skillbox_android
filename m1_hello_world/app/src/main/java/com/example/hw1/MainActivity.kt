package com.example.hw1

import android.annotation.SuppressLint
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.view.isVisible
import com.example.hw1.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {


    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(R.layout.activity_main)
        setContentView(binding.root)

        var count = 0

        fun trackCount(count: Int) {
            when (count) {
                0 -> {
                    binding.mainText.text = getText(R.string.empty_text)
                    binding.mainText.setTextColor(Color.parseColor("#00ff00"))
                    binding.minus.isEnabled = true
                    binding.undo.isVisible = false
                }
                in 1..49 -> {
                    val placesLeft = 50 - count
                    binding.mainText.text =
                        getText(R.string.places_left).toString() + " " + placesLeft.toString()
                    binding.mainText.setTextColor(Color.parseColor("#0000FF"))
                    binding.undo.isVisible = false
                }
                50 -> {
                    binding.mainText.text = getText(R.string.too_much)
                    binding.undo.isVisible = true
                    binding.mainText.setTextColor(Color.parseColor("#FF0000"))
                }
            }
        }

        trackCount(count)

        binding.plus.setOnClickListener {
            if (count < 50) count++
            binding.count.text = count.toString()
            trackCount(count)
        }

        binding.minus.setOnClickListener {
            if (count > 0) count--
            binding.count.text = count.toString()
            trackCount(count)
        }

        binding.undo.setOnClickListener {
            count = 0
            binding.count.text = count.toString()
            trackCount(count)
        }
    }


}