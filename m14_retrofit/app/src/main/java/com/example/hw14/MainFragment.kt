package com.example.hw14

import android.app.Service
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.lifecycle.lifecycleScope
import coil.load
import com.example.hw14.databinding.FragmentMainBinding

class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMainBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

       getInfo()

        binding.refreshInfo.setOnClickListener {
            getInfo()
        }
    }

    fun getInfo(){
        lifecycleScope.launchWhenStarted {
            val response = RetrofitInstance.searchApi.getUserInfo()
            val info = response.body()!!.results[0]
            binding.name.text = info.name.first
            binding.surname.text = info.name.last
            binding.title.text = info.name.title
            binding.email.text = info.email
            binding.phone.text = info.phone
            binding.avatar.load(response.body()!!.results[0].picture.large)
        }

    }

}