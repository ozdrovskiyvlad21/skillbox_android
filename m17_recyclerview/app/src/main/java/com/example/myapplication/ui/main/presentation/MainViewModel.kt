package com.example.myapplication.ui.main.presentation

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.example.myapplication.ui.main.data.NasaPhotosRepository
import com.example.myapplication.ui.main.domain.NasaInfoUseCase
import com.example.myapplication.ui.main.entity.NasaMarsPhotosList
import com.example.myapplication.ui.main.presentation.marsPhotosList.PhotoPaged
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainViewModel : ViewModel() {

    @Inject
    lateinit var useCase: NasaInfoUseCase

    @Inject
    lateinit var paged: PhotoPaged

    private val _marsPhotos = MutableStateFlow<List<NasaMarsPhotosList>>(emptyList())
    val marsPhotos = _marsPhotos.asStateFlow()

    var pageMovies: Flow<PagingData<NasaMarsPhotosList>> = Pager(
        config = PagingConfig(pageSize = 10),
        pagingSourceFactory = { paged }
    ).flow.cachedIn(viewModelScope)

     fun loadPhotos(useCase: NasaInfoUseCase){
        viewModelScope.launch(Dispatchers.IO) {
            kotlin.runCatching {
                useCase.execute(1000, 1)
            }.fold(
                onSuccess = { _marsPhotos.value = it},
                onFailure = { Log.d("MainViewModel", it.message ?: "")}
            )
        }
    }

    fun updateList(){
        loadPhotos(useCase)
    }
}