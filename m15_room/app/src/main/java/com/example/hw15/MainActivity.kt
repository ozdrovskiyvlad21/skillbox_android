package com.example.hw15

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.hw15.ui.main.App
import com.example.hw15.ui.main.DictionaryDao
import com.example.hw15.ui.main.MainFragment

class MainActivity : AppCompatActivity() {

    lateinit var dictionaryDao : DictionaryDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        dictionaryDao = getDao()

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, MainFragment.newInstance())
                .commitNow()
        }

    }

    private fun getDao(): DictionaryDao {
        return (application as App).db.dictionaryDao()
    }
}