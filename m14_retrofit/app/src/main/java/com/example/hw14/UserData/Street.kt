package com.example.hw14.UserData

data class Street(
    val name: String,
    val number: Int
)