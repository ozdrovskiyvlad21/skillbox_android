package com.example.myapplication.ui.main.entity

data class Camera(
    val full_name: String,
    val id: Int,
    val name: String,
    val rover_id: Int
)