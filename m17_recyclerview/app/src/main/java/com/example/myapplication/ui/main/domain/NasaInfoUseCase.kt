package com.example.myapplication.ui.main.domain

import com.example.myapplication.ui.main.data.NasaPhotosRepository
import com.example.myapplication.ui.main.entity.NasaMarsPhotosList
import com.example.myapplication.ui.main.entity.NasaPhotosResponse
import javax.inject.Inject

class NasaInfoUseCase @Inject constructor(private val usefulActivitiesRepository: NasaPhotosRepository){

    suspend fun execute(sol: Int, page: Int): List<NasaMarsPhotosList> {
        return  usefulActivitiesRepository.getNasaInfo(sol, page)
    }
}