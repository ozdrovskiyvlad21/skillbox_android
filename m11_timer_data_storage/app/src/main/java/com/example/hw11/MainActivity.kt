package com.example.hw11

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.hw11.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private var repository = Repository()

    private lateinit var prefs: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

       setText()

        binding.saveBt.setOnClickListener {
            repository.saveText(binding.editText.text.toString())
            setText()
        }

        binding.deleteBt.setOnClickListener {
            repository.clearText()
            setText()
        }

    }

    fun setText(){
        binding.text.text = repository.getText(this)
    }
}