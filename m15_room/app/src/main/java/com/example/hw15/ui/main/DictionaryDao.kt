package com.example.hw15.ui.main

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface DictionaryDao {

    @Query("SELECT * FROM dictionary")
    fun getAll(): Flow<List<Dictionary>>

    @Insert(entity = Dictionary::class)
    fun insert(dictionary: NewWord)

    @Delete
    fun delete(dictionary: Dictionary)

    @Update
    fun update(dictionary: Dictionary)
}