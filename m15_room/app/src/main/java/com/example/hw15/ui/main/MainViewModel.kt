package com.example.hw15.ui.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import java.util.concurrent.Flow

class MainViewModel(private val dictionaryDao: DictionaryDao) : ViewModel() {

    val allWords = this.dictionaryDao.getAll()
        .stateIn(
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(5000L),
            initialValue = emptyList()
        )

    fun addWord(word: String) {
        viewModelScope.launch(Dispatchers.IO) {
            if (allWords.value.isNotEmpty()) {
                for (i in 0 until allWords.value.size) {
                    if (word == allWords.value[i].word) {
                        val sameWord = allWords.value[i].copy(
                            count = allWords.value[i].count + 1
                        )
                        dictionaryDao.update(sameWord)
                    } else {
                        dictionaryDao.insert(
                            NewWord(
                                word = word
                            )
                        )
                    }
                }
            } else {
                dictionaryDao.insert(
                    NewWord(
                        word = word
                    )
                )
            }
        }
    }

    fun deleteAll() {
        viewModelScope.launch {
            allWords.value.lastOrNull()?.let { dictionaryDao.delete(it) }
        }
    }
}