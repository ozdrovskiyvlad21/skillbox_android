package com.example.skillbox_hw_quiz.ui.main

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.skillbox_hw_quiz.R
import com.example.skillbox_hw_quiz.databinding.MainFragmentBinding
import com.example.skillbox_hw_quiz.quiz.Quiz
import com.example.skillbox_hw_quiz.quiz.QuizStorage

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private var _binding: MainFragmentBinding? = null

    private val binding get() = _binding!!

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = MainFragmentBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val q = QuizStorage.getQuiz(QuizStorage.Locale.Ru)
        binding.Q1.text = q.questions[0].question
        binding.q1Answer1.text = q.questions[0].answers[0]
        binding.q1Answer2.text = q.questions[0].answers[1]
        binding.q1Answer3.text = q.questions[0].answers[2]
        binding.q1Answer4.text = q.questions[0].answers[3]

        binding.firstQuestion.setOnCheckedChangeListener { buttonId, _ ->

            when (buttonId) {
                binding.q1Answer1 -> QuizStorage.answer(q, listOf(1))
                binding.q1Answer2 -> QuizStorage.answer(q, listOf(2))
                binding.q1Answer3 -> QuizStorage.answer(q, listOf(3))
                binding.q1Answer4 -> QuizStorage.answer(q, listOf(4))

            }
        }

        binding.Q2.text = q.questions[1].question
        binding.q2Answer1.text = q.questions[1].answers[0]
        binding.q2Answer2.text = q.questions[1].answers[1]
        binding.q2Answer3.text = q.questions[1].answers[2]
        binding.q2Answer4.text = q.questions[1].answers[3]

        binding.firstQuestion.setOnCheckedChangeListener { buttonId, _ ->

            when (buttonId) {
                binding.q2Answer1 -> QuizStorage.answer(q, listOf(1))
                binding.q2Answer2 -> QuizStorage.answer(q, listOf(2))
                binding.q2Answer3 -> QuizStorage.answer(q, listOf(3))
                binding.q2Answer4 -> QuizStorage.answer(q, listOf(4))

            }
        }

        binding.Q3.text = q.questions[2].question
        binding.q3Answer1.text = q.questions[2].answers[0]
        binding.q3Answer2.text = q.questions[2].answers[1]
        binding.q3Answer3.text = q.questions[2].answers[2]
        binding.q3Answer4.text = q.questions[2].answers[3]

        binding.firstQuestion.setOnCheckedChangeListener { buttonId, _ ->

            when (buttonId) {
                binding.q1Answer1 -> QuizStorage.answer(q, listOf(1))
                binding.q1Answer2 -> QuizStorage.answer(q, listOf(2))
                binding.q1Answer3 -> QuizStorage.answer(q, listOf(3))
                binding.q1Answer4 -> QuizStorage.answer(q, listOf(4))

            }
        }

        binding.sendBt.setOnClickListener {
             findNavController().navigate(R.id.action_main_fragment_to_result_fragment)
            onDestroy()
        }

        binding.backBt.setOnClickListener {
            findNavController().navigate(R.id.action_main_fragment_to_welcome_fragment)
        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        // TODO: Use the ViewModel
    }


}