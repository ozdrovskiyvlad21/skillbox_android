package com.example.skillbox_hw_quiz.ui.main

import android.graphics.Color.alpha
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.commit
import androidx.navigation.fragment.findNavController
import com.example.skillbox_hw_quiz.R
import com.example.skillbox_hw_quiz.databinding.FragmentResultBinding
import com.example.skillbox_hw_quiz.databinding.MainFragmentBinding
import com.example.skillbox_hw_quiz.quiz.QuizStorage

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private var _binding: MainFragmentBinding? = null

    private val binding get() = _binding!!

    private lateinit var viewModel: MainViewModel

    private val quizStorage = QuizStorage.getQuiz(QuizStorage.Locale.Ru)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = MainFragmentBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.Q1.text = quizStorage.questions[0].question
        binding.q1Answer1.text = quizStorage.questions[0].answers[0]
        binding.q1Answer2.text = quizStorage.questions[0].answers[1]
        binding.q1Answer3.text = quizStorage.questions[0].answers[2]
        binding.q1Answer4.text = quizStorage.questions[0].answers[3]

        binding.firstQuestion.animate().apply {
            duration = 2000
            alpha(0f)
            alphaBy(1f)
        }.start()
        binding.secondQuestion.animate().apply {
            duration = 4000
            alpha(1f)
        }.start()
        binding.thirdQuestion.animate().apply {
            duration = 6000
            alpha(1f)
        }.start()


        binding.Q2.text = quizStorage.questions[1].question
        binding.q2Answer1.text = quizStorage.questions[1].answers[0]
        binding.q2Answer2.text = quizStorage.questions[1].answers[1]
        binding.q2Answer3.text = quizStorage.questions[1].answers[2]
        binding.q2Answer4.text = quizStorage.questions[1].answers[3]


        binding.Q3.text = quizStorage.questions[2].question
        binding.q3Answer1.text = quizStorage.questions[2].answers[0]
        binding.q3Answer2.text = quizStorage.questions[2].answers[1]
        binding.q3Answer3.text = quizStorage.questions[2].answers[2]
        binding.q3Answer4.text = quizStorage.questions[2].answers[3]


        binding.sendBt.setOnClickListener {
            val bundle = Bundle().apply {
                this.putString("answer", checkForAnswers())
            }
            findNavController().navigate(R.id.action_main_fragment_to_result_fragment, bundle)
            onDestroy()
        }

        binding.backBt.setOnClickListener {
            findNavController().navigate(R.id.action_main_fragment_to_welcome_fragment)
        }


        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true /* enabled by default */) {
                override fun handleOnBackPressed() {
                    findNavController().navigate(R.id.action_main_fragment_to_welcome_fragment)
                    onDestroy()
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        // TODO: Use the ViewModel
    }

    private fun checkForAnswers(): String {
        val index = mutableListOf<Int>()
        val listOfViewsWithQuestions =
            listOf(binding.firstQuestion, binding.secondQuestion, binding.thirdQuestion)
        for (question in quizStorage.questions.indices) {
            when (listOfViewsWithQuestions[question].checkedRadioButtonId) {
                R.id.q1_answer1 -> index.add(0)
                R.id.q1_answer2 -> index.add(1)
                R.id.q1_answer3 -> index.add(2)
                R.id.q1_answer4 -> index.add(3)

                R.id.q2_answer1 -> index.add(0)
                R.id.q2_answer2 -> index.add(1)
                R.id.q2_answer3 -> index.add(2)
                R.id.q2_answer4 -> index.add(3)

                R.id.q3_answer1 -> index.add(0)
                R.id.q3_answer2 -> index.add(1)
                R.id.q3_answer3 -> index.add(2)
                R.id.q3_answer4 -> index.add(3)
                else -> {}
            }
        }
        return QuizStorage.answer(quizStorage, index)
    }


}