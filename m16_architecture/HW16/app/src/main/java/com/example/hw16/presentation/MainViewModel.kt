package com.example.hw16.presentation

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.hw16.domain.GetUsefulActivityUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainViewModel @Inject constructor
    (private val getUsefulActivityUseCase: GetUsefulActivityUseCase) : ViewModel() {

    var state = MutableStateFlow(String.toString())

    fun reloadUsefulActivity() {
        viewModelScope.launch {
            val activity = getUsefulActivityUseCase.execute()
            state.value = activity.activity
        }
    }
}