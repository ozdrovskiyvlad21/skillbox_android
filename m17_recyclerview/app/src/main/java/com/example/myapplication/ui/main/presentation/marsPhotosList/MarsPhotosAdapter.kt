package com.example.myapplication.ui.main.presentation.marsPhotosList

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.myapplication.databinding.MarsItemPhotoBinding
import com.example.myapplication.ui.main.entity.NasaMarsPhotosList

class MarsPhotosAdapter(
    private val onClick: (NasaMarsPhotosList) -> Unit
) : RecyclerView.Adapter<MarsPhotosViewHolder>(){


    private var values: List<NasaMarsPhotosList> = emptyList()

    fun getData(data: List<NasaMarsPhotosList>){
        this.values = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MarsPhotosViewHolder {
       val binding = MarsItemPhotoBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MarsPhotosViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MarsPhotosViewHolder, position: Int) {
        val item = values.getOrNull(position)
        with(holder.binding){
            roverText.text = item?.rover?.name
            dataText.text = item?.earth_date
            item?.let {
                Glide
                    .with(marsPhotos.context)
                    .load(it.img_src)
                    .into(marsPhotos)
            }
        }

        holder.binding.root.setOnClickListener {
            item?.let(onClick)
        }
    }

    override fun getItemCount(): Int = values.size
}

class MarsPhotosViewHolder(val binding: MarsItemPhotoBinding) :
    RecyclerView.ViewHolder(binding.root)