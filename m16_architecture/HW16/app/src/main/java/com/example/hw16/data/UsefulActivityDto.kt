package com.example.hw16.data

import com.example.hw16.entity.UsefulActivity

class UsefulActivityDto(
    override val accessibility: Double,
    override val activity: String,
    override val key: String,
    override val link: String,
    override val participants: Double,
    override val price: Double,
    override val type: String,
) : UsefulActivity
