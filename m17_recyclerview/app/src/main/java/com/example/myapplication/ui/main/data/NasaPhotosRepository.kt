package com.example.myapplication.ui.main.data

import com.example.myapplication.ui.main.entity.NasaMarsPhotosList
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.Field
import retrofit2.http.GET
import retrofit2.http.Query
import javax.inject.Inject

class NasaPhotosRepository @Inject constructor() {
    suspend fun getNasaInfo(sol: Int, page: Int): List<NasaMarsPhotosList> {
        return RetrofitServices.searchApi.getUsefulActivity(sol = sol, page = page).photos
    }

}

interface SearchApi {

    private companion object{
        private const val NASA_API_KEY = "zlNAVyx8R0cR7agLejnKxtMqvtldccus68Bg7de7"
    }

    @GET("photos?")
    suspend fun getUsefulActivity(
        @Query("sol") sol: Int = 1000,
        @Query("page") page: Int = 1,
        @Query("api_key") key: String = NASA_API_KEY,
    ): NasaPhotosResponseDto
}

object RetrofitServices {

    private const val BASE_URL = "https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/"

    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(MoshiConverterFactory.create())
        .build()

    val searchApi: SearchApi = retrofit.create(SearchApi::class.java)
}