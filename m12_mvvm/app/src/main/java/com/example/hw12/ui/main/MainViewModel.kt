package com.example.hw12.ui.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.hw12.R
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {

    private val _state = MutableStateFlow<State>(State.Success)
    val state = _state.asStateFlow()
    private val _error = Channel<String>()
    val error = _error.receiveAsFlow()
    var finalMessage: String? = "Здесь будет отображаться результат запроса"

    fun onButtonClickAction(searchText: String) {
        viewModelScope.launch {
            val isTooShort = searchText.length < 3
            val isInProgress = _state.value == State.Loading
            if (!isTooShort && !isInProgress) {
                _state.value = State.Loading
                delay(5_000)
                finalMessage = "По запросу <${searchText}> ничего не найдено"
                _state.value = State.Success
            } else {
                var errorShort: String? = null
                if (isTooShort) {
                    errorShort = "Too short request"
                }
                _state.value = State.Error(errorShort)
                _error.send("Error")
            }
        }
    }

}