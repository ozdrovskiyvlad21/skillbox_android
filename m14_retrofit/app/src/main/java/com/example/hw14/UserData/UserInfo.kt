package com.example.hw14.UserData

data class UserInfo(
    val info: Info,
    val results: List<Result>
)