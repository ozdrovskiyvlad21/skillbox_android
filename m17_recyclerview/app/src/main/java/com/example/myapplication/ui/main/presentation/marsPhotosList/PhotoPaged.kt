package com.example.myapplication.ui.main.presentation.marsPhotosList

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.myapplication.ui.main.domain.NasaInfoUseCase
import com.example.myapplication.ui.main.entity.NasaMarsPhotosList
import javax.inject.Inject

class PhotoPaged @Inject constructor(private val useCase: NasaInfoUseCase) : PagingSource<Int, NasaMarsPhotosList>() {

    override fun getRefreshKey(state: PagingState<Int, NasaMarsPhotosList>): Int = FIRST_PAGE

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, NasaMarsPhotosList> {
        val page = params.key ?: FIRST_PAGE
        return kotlin.runCatching {
            useCase.execute(1000, page)
        }.fold(
            onSuccess = {
                LoadResult.Page(
                    data = it,
                    prevKey = null,
                    nextKey = if (it.isEmpty()) null else page + 1
                )
            },
            onFailure = { LoadResult.Error(it) }
        )
    }

    companion object {
        private const val FIRST_PAGE = 1
    }

}